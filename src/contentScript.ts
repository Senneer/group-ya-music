import * as Action from "constants/ActionTypes";

connectPageScript();

function sendCustomEvent(action: any) {
    const event = new CustomEvent(action);

    window.dispatchEvent(event);
}

function manageChromeMessages(message: any) {
    switch (message.action) {
        case Action.SWITCH_SONG:
            return sendCustomEvent(Action.SWITCH_SONG);
        case Action.TOGGLE_PLAY:
            return sendCustomEvent(Action.TOGGLE_PLAY);
        case Action.TOGGLE_LIKE:
            return sendCustomEvent(Action.TOGGLE_LIKE);
        default:
            break;
    }
}

function manageWindowMessages(event: any) {
    const { data } = event;

    switch (data.action) {
        case Action.YA_STATE_CHANGED:
            chrome.runtime.sendMessage({
                action: Action.YA_STATE_CHANGED,
                track: data.track,
                isPlaying: data.isPlaying
            });
            break;
        case Action.GET_INIT_DATA_FROM_PAGE:
            chrome.runtime.sendMessage({
                action: Action.GET_INIT_DATA_FROM_PAGE,
                track: data.track,
                isPlaying: data.isPlaying
            });
            break;
        default:
    }
}

if (!(<any>Window).areEventManagersAdded) {
    chrome.runtime.onMessage.addListener(manageChromeMessages);
    window.addEventListener("message", manageWindowMessages);

    (<any>Window).areEventManagersAdded = true;
}

function connectPageScript() {
    const scriptTag = document.createElement("script");

    scriptTag.src = chrome.extension.getURL("pageScript.js");
    (document.head || document.documentElement).appendChild(scriptTag);

    window.addEventListener("beforeunload", () => {
        chrome.runtime.sendMessage({ action: Action.PAGE_RELOADED });
    });

    if (scriptTag.parentNode) {
        scriptTag.parentNode.removeChild(scriptTag);
    }
}
