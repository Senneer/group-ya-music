import * as React from "react";
import * as ReactDOM from "react-dom";
import { App } from "containers";

setInterval(() => {
    chrome.runtime.sendMessage({ action: "DUCK" });
}, 7000);

ReactDOM.render(<App />, document.getElementById("root"));
