import * as React from "react";
import { AppContext, defaultAppState } from "context/AppStateContext";
import { IAppState, Page } from "interfaces";
import { IPInput, Player, ErrorPage } from "components";
import * as Action from "constants/ActionTypes";
import "./App.css";

export class App extends React.Component<{}, IAppState> {
    state = defaultAppState;

    componentDidMount() {
        chrome.runtime.sendMessage(
            { action: Action.GET_INITIAL_STATE },
            res => {
                this.setState(res);
            }
        );

        chrome.runtime.onMessage.addListener(message => {
            if (message.action === Action.UPDATE_STATE) {
                this.setState(message.store);
            }
        });
    }

    handleServerConnect = (ip: string) => {
        chrome.runtime.sendMessage({
            action: Action.JOIN,
            ip
        });
    };

    renderPage = () => {
        const {
            page,
            currentTrack,
            role,
            isPlaying,
            peopleInRoom,
            votes,
            isVoted
        } = this.state;

        switch (page) {
            case Page.Error:
                return <ErrorPage />;
            case Page.Player:
                return (
                    <Player
                        currentTrack={currentTrack}
                        role={role}
                        isPlaying={isPlaying}
                        peopleInRoom={peopleInRoom}
                        votes={votes}
                        isVoted={isVoted}
                    />
                );
            case Page.IPInput:
            default:
                return (
                    <IPInput handleServerConnect={this.handleServerConnect} />
                );
        }
    };

    render() {
        return (
            <AppContext.Provider value={this.state}>
                <div className="wrapper">{this.renderPage()}</div>
            </AppContext.Provider>
        );
    }
}
