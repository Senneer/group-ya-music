import * as React from 'react';
import { IAppState, Page } from 'interfaces';

export const defaultAppState = {
    role: null,
    page: Page.IPInput,
    currentTrack: null,
    isPlaying: false,
    peopleInRoom: 0,
    votes: 0,
    isVoted: false,
};

export const AppContext = React.createContext<IAppState>(defaultAppState);
