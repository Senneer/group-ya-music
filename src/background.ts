import { defaultAppState } from "context/AppStateContext";
import * as io from "socket.io-client";
import { IAppState, Page, Role } from "interfaces";
import * as Action from "constants/ActionTypes";

const URL_TEMPLATE = "*://music.yandex.ru/*";

let appState: IAppState = Object.assign({}, defaultAppState);
let tab: chrome.tabs.Tab | null = null;
let socket: any;

function createSocket(ip: string) {
    const url = `http://${ip}:3231`;

    return io(url, {
        reconnection: true,
        reconnectionDelay: 500,
        reconnectionAttempts: 5
    });
}

const getTargetTab = () => {
    return new Promise(resolve => {
        chrome.tabs.query({ url: URL_TEMPLATE }, ([targetTab]) => {
            if (!targetTab) {
                return resolve();
            }

            resolve(targetTab);
        });
    });
};

function updateState() {
    chrome.runtime.sendMessage({
        action: Action.UPDATE_STATE,
        store: appState
    });
}

function manageSocket(message: any, _: any, sendResponse: any) {
    switch (message.action) {
        case Action.GET_INITIAL_STATE:
            sendResponse(appState);
            break;
        case Action.JOIN:
            socket = createSocket(message.ip);

            socket.emit(Action.JOIN);

            socket.on(Action.ENABLE_HOST, async (message: any) => {
                const targetTab = (await getTargetTab()) as chrome.tabs.Tab;
                if (message) {
                    appState.currentTrack = message.track;
                    appState.isPlaying = message.isPlaying;
                }

                if (targetTab) {
                    tab = targetTab;
                    appState.page = Page.Player;
                    appState.role = Role.Host;

                    if (tab && tab.id) {
                        chrome.tabs.executeScript(tab.id, {
                            file: "contentScript.js"
                        });
                    }
                } else {
                    tab = null;
                    appState.page = Page.Error;
                    socket.close();
                }

                updateState();

                if (appState.page === Page.Error) {
                    appState.page = Page.IPInput;
                }
            });

            socket.on(Action.ENABLE_LISTENER, (message: any) => {
                appState.page = Page.Player;
                appState.role = Role.Listener;
                if (message) {
                    appState = {
                        ...appState,
                        ...message
                    };
                }
                updateState();
            });

            socket.on(Action.LOGOUT, () => {
                appState = Object.assign({}, defaultAppState);
                chrome.browserAction.setBadgeText({ text: "" });
                updateState();
                socket.close();
            });

            socket.on(Action.SWITCH_SONG, () => {
                if (tab && tab.id) {
                    chrome.tabs.sendMessage(tab.id, {
                        action: Action.SWITCH_SONG
                    });
                }
            });

            socket.on(Action.YA_STATE_CHANGED, (message: any) => {
                if (message) {
                    if (
                        appState.currentTrack &&
                        appState.currentTrack.title !== message.track.title
                    ) {
                        appState.isVoted = false;
                    }

                    appState.currentTrack = message.track;
                    appState.votes = message.votes;
                    appState.isPlaying = message.isPlaying;
                }

                chrome.browserAction.setBadgeText({
                    text: message.votes ? `${message.votes}` : ""
                });
                updateState();
            });

            socket.on(Action.TOGGLE_PLAY, (message: any) => {
                if (message) {
                    appState.isPlaying = message.isPlaying;
                    if (tab && tab.id && appState.role === Role.Host) {
                        chrome.tabs.sendMessage(tab.id, {
                            action: Action.TOGGLE_PLAY
                        });
                    }

                    updateState();
                }
            });

            socket.on(Action.GET_INIT_DATA_FROM_PAGE, (message: any) => {
                if (message) {
                    appState.currentTrack = message.track;
                    appState.isPlaying = message.isPlaying;

                    updateState();
                }
            });

            socket.on(Action.UPDATE_PEOPLE_COUNT, (message: any) => {
                if (message) {
                    appState.peopleInRoom = message;

                    updateState();
                }
            });

            socket.on(Action.VOTE_FOR_NEXT_SONG, (message: any) => {
                if (message) {
                    appState.votes = message.votes;
                    chrome.browserAction.setBadgeText({
                        text: message.votes ? `${message.votes}` : ""
                    });

                    updateState();
                }
            });

            socket.on(Action.ADD_VOTE, () => {
                appState.isVoted = true;

                updateState();
            });

            socket.on(Action.LIKE, () => {
                if (tab && tab.id && appState.role === Role.Host) {
                    chrome.tabs.sendMessage(tab.id, {
                        action: Action.TOGGLE_LIKE
                    });
                }
            });

            socket.on("reconnect_failed", () => {
                console.log("Reconnected failed");

                appState = Object.assign({}, defaultAppState);
                chrome.browserAction.setBadgeText({ text: "" });
                updateState();
                socket.close();
            });
            break;
        case Action.VOTE_FOR_NEXT_SONG:
            socket.emit(Action.VOTE_FOR_NEXT_SONG);
            break;
        case Action.YA_STATE_CHANGED:
            socket.emit(Action.YA_STATE_CHANGED, {
                track: message.track,
                isPlaying: message.isPlaying
            });
            break;
        case Action.PAGE_RELOADED:
        case Action.LOGOUT:
            socket.emit(Action.LOGOUT);
            break;
        case Action.TOGGLE_PLAY:
            socket.emit(Action.TOGGLE_PLAY);
            break;
        case Action.GET_INIT_DATA_FROM_PAGE:
            socket.emit(Action.GET_INIT_DATA_FROM_PAGE, {
                track: message.track,
                isPlaying: message.isPlaying
            });
            break;
        case Action.LIKE:
            socket.emit(Action.LIKE);
        default:
    }
}

chrome.runtime.onMessage.addListener(manageSocket);
