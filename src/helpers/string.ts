export const isIp = (ip: string): boolean => {
    const trimedIp = ip.trim();

    const ipValues = trimedIp.split('.');

    if (ipValues.length !== 4) return false;

    const isIpValid = ipValues.every(value => {
        const isCorrectNumber = value.length > 0 && value.length < 4 && +value >= 0 && +value < 256;

        return isCorrectNumber;
    });

    return isIpValid;
};
