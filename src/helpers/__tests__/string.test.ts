import { isIp } from '../string';

describe('String helper', () => {
    describe('Фукнция isIp', () => {
        test('Если пришла пустая строка, вернет false', () => {
            const value = '';
            const result = isIp(value);

            expect(result).toBe(false);
        });

        test('Если пришел корректный IP, вернет true', () => {
            const value = '192.54.0.15';
            const result = isIp(value);

            expect(result).toBe(true);
        });

        test('Если пришел неполный IP, вернет false', () => {
            const value = '192.0.54';
            const result = isIp(value);

            expect(result).toBe(false);
        });

        test('Если пришел некорректный IP, вернет false', () => {
            const value = '192.255.256.1';
            const result = isIp(value);

            expect(result).toBe(false);
        });

        test('Если пришел не IP, вернет false', () => {
            const value = 'Это точно не IP';
            const result = isIp(value);

            expect(result).toBe(false);
        });
    });
});
