import * as express from "express";
import { createServer } from "http";
import * as socketIO from "socket.io";
import * as Action from "../constants/ActionTypes";
import { Track } from "../interfaces";

const app = express();
const server = createServer(app);
const io = socketIO(server, {
    serveClient: false
})

const VOTES_COUNT_TO_SWITCH = 2;

const PORT = process.env.PORT || 3231;

let hostID: string | null = null;
let currentTrack: Track | null = null;
let isPlaying = false;
let votes: string[] = ['asd'];

function manageSocket(socket: SocketIO.Socket) {
    const { id } = socket;
    console.log(`\nConnected socket with ID: ${id}`);

    const connectedClients = getConnectedClients();
    console.log(
        `List of connected clients: (${connectedClients.length})\n`,
        connectedClients
    );

    io.emit(Action.UPDATE_PEOPLE_COUNT, connectedClients.length);

    socket.on(Action.JOIN, () => {
        if (hostID) {
            io.to(id).emit(Action.ENABLE_LISTENER, {
                currentTrack,
                isPlaying,
                votes: votes.length
            });
            return;
        }

        hostID = id;
        io.to(id).emit(Action.ENABLE_HOST, {
            track: currentTrack,
            isPlaying
        });
    });

    socket.on(Action.VOTE_FOR_NEXT_SONG, () => {
        const isClientVoted = votes.some(vote => vote === id);

        if (!isClientVoted) {
            votes.push(id);
        }

        io.emit(Action.VOTE_FOR_NEXT_SONG, { votes: votes.length });
        socket.emit(Action.ADD_VOTE);

        if (hostID && votes.length >= VOTES_COUNT_TO_SWITCH) {
            io.to(hostID).emit(Action.SWITCH_SONG);

            votes = [];
        }
    });

    socket.on(Action.YA_STATE_CHANGED, message => {
        // Обнулим голоса за переключение трека,
        // если трек сменился
        if (!currentTrack || message.track.title !== currentTrack.title) {
            currentTrack = Object.assign({}, message.track);
            votes = [];
        }

        if (currentTrack && message.track.liked !== currentTrack.liked) {
            currentTrack = Object.assign({}, message.track);
        }

        isPlaying = message.isPlaying;

        io.emit(Action.YA_STATE_CHANGED, {
            track: currentTrack,
            isPlaying: isPlaying,
            votes: votes.length
        });
    });

    socket.on(Action.TOGGLE_PLAY, () => {
        isPlaying = !isPlaying;
        io.emit(Action.TOGGLE_PLAY, { isPlaying });
    });

    socket.on(Action.LOGOUT, () => {
        votes = votes.filter(userId => userId !== id);

        // Если отключился хост, то отключим всех слушателей
        if (hostID && hostID === id) {
            hostID = null;
            io.emit(Action.LOGOUT);
            return;
        }

        socket.emit(Action.LOGOUT);
    });

    socket.on(Action.GET_INIT_DATA_FROM_PAGE, data => {
        currentTrack = data.track;
        isPlaying = data.isPlaying;

        if (hostID) {
            io.to(hostID).emit(Action.GET_INIT_DATA_FROM_PAGE, {
                track: currentTrack,
                isPlaying
            });
        }
    });

    socket.on(Action.LIKE, () => {
        if (hostID) {
            io.to(hostID).emit(Action.LIKE);
        }
    });

    socket.on("disconnect", reason => {
        console.log(`\n${id} disconnected. Reason:\n${reason}`);

        if (hostID && hostID === id) {
            hostID = null;
            io.emit(Action.LOGOUT);
        }

        const connectedClients = getConnectedClients();

        io.emit(Action.UPDATE_PEOPLE_COUNT, connectedClients.length);
        console.log(
            `List of connected clients: (${connectedClients.length})\n`,
            connectedClients
        );
    });
}

io.on("connection", manageSocket);

server.listen(PORT, () => {
    console.log(`Connected on port: ${PORT}`);
});

function getConnectedClients() {
    const { connected } = io.sockets.clients(() => {});

    return Object.keys(connected);
}
