import * as React from 'react';
import { IPInputState } from "interfaces";
import './IPInput.css';
import { isIp } from 'helpers/string';

interface IProps {
    handleServerConnect: (ip: string) => void;
};

const ENTER_CODE = 13;
const ErrorMsg = {
    EMPTY: 'Введите IP',
    NOT_IP: 'Некорректный IP',
};

export class IPInput extends React.PureComponent<IProps, IPInputState> {
    state = {
        value: '',
        error: '',
    };

    componentDidMount() {
        chrome.storage.local.get('lastIp', (storage) => {
            if (storage.lastIp) {
                this.setState({ value: storage.lastIp });
            }
        });

        window.addEventListener('keypress', this.handleKeyPress);
    }

    componentWillUnmount() {
        window.removeEventListener('keypress', this.handleKeyPress);
    }

    handleKeyPress = (event: any) => {
        if (event.keyCode === ENTER_CODE) {
            this.handleFormSubmit();
        }
    }

    handleInputChange = (event: any) => {
        const { value } = event.target;

        this.setState({ value, error: '' });
    }

    handleFormSubmit = () => {
        const { value } = this.state;

        if (!value) {
            this.setState({ error: ErrorMsg.EMPTY });
            return;
        }

        const ip = value.trim();
        const isIpValid = isIp(ip);

        if (!isIpValid) {
            this.setState({ error: ErrorMsg.NOT_IP });
            return;
        }

        chrome.storage.local.set({ lastIp: ip });
        this.props.handleServerConnect(ip);
    }

    render() {
        const { value, error } = this.state;

        return (
            <div className='IPInput'>
                <input
                    type="text"
                    className='IPInput__inp'
                    placeholder='Введите ip сервера'
                    autoFocus
                    value={value}
                    onChange={this.handleInputChange}
                />
                <button className='IPInput__btn' onClick={this.handleFormSubmit}>Подключиться</button>
                {error && <p className='IPInput__error'>{error}</p>}
            </div>
        );
    }
}
