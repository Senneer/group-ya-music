import * as React from 'react';
import './TrackInfo.css';
import { TrackState } from 'interfaces';

interface IProps {
    currentTrack: TrackState;
}

const COVER_SIZE = '50x50';

export const TrackInfo = (props: IProps) => {
    const { currentTrack } = props;

    if (!currentTrack) return null;

    const { artists, title, link, cover } = currentTrack;
    const songLink = `https://music.yandex.ru${link}`;
    const artistTitle = artists && artists[0] && artists[0].title || '';
    const coverLink = cover
        ? `https://${cover.replace('%%', COVER_SIZE)}`
        : chrome.runtime.getURL('track.png');

    return (
        <div className="trackInfo">
            <img src={coverLink} alt={artistTitle} className='trackInfo__cover' />
            <div className="traclInfo__main">
                {artistTitle &&
                    <p className="trackInfo__mainArtist">{artistTitle}</p>
                }
                {title &&
                    <a href={songLink} className="trackInfo__mainTitle" target='_blank' rel='noopener'>{title}</a>
                }
            </div>
        </div>
    );
};
