import * as React from "react";
import * as Action from "constants/ActionTypes";
import "./Player.css";
import { TrackState, Role, RoleState } from "interfaces";
import {
    TrackInfo,
    Logout,
    Play,
    Pause,
    Next,
    Man,
    Megaphone,
    Heart
} from "components";

interface IProps {
    currentTrack: TrackState;
    role: RoleState;
    isPlaying: boolean;
    peopleInRoom: number;
    votes: number;
    isVoted: boolean;
}

export class Player extends React.PureComponent<IProps, {}> {
    handleVoteClick = () => {
        const { isVoted } = this.props;

        if (!isVoted) {
            chrome.runtime.sendMessage({ action: Action.VOTE_FOR_NEXT_SONG });
        }
    };

    handleLogoutClick = () => {
        chrome.runtime.sendMessage({ action: Action.LOGOUT });
    };

    handlePlayToggle = () => {
        chrome.runtime.sendMessage({ action: Action.TOGGLE_PLAY });
    };

    handleLikeClick = () => {
        chrome.runtime.sendMessage({ action: Action.LIKE });
    };

    renderHeader = () => {
        const { role, peopleInRoom, votes } = this.props;

        return (
            <header className="player__header">
                {role === Role.Host && (
                    <img
                        src={chrome.runtime.getURL("crown.svg")}
                        alt="Хост"
                        className="player__headerRole"
                        title="Все вокруг слушают ваш плейлист"
                    />
                )}
                <div className="player__headerInfo">
                    <div
                        className="peopleCount"
                        title="Количество людей в комнате"
                    >
                        <Man className="peopleCount__icon" />
                        <span className="peopleCount__number">
                            {peopleInRoom}
                        </span>
                    </div>
                    {votes > 0 && (
                        <div
                            className="votes"
                            title="Кто-то проголосовал за переключение"
                        >
                            <Megaphone className="votes__icon" />
                            {votes > 1 && (
                                <span className="votes__count">{votes}</span>
                            )}
                        </div>
                    )}
                </div>
            </header>
        );
    };

    renderControls = () => {
        const { isPlaying, isVoted, currentTrack } = this.props;

        const voteBtnStyles = ["player__ctrlsVote", isVoted && "_voted"]
            .filter(x => x)
            .join(" ");

        const likeBtnStyles = [
            "player__ctrlsLike",
            currentTrack && currentTrack.liked && "_liked"
        ]
            .filter(x => x)
            .join(" ");

        const voteBtnTitle = isVoted
            ? "Ваш голос учтён"
            : "Голосовать за переключение";

        return (
            <div className="player__ctrls">
                <button
                    className="player__ctrlsPlay"
                    aria-label={
                        isPlaying ? "Воспроизвести" : "Поставить на паузу"
                    }
                    onClick={this.handlePlayToggle}
                >
                    {isPlaying ? (
                        <Pause
                            className="player__ctrlsPlayIcon"
                            aria-hidden="true"
                        />
                    ) : (
                        <Play
                            className="player__ctrlsPlayIcon"
                            aria-hidden="true"
                        />
                    )}
                </button>
                <button
                    className={voteBtnStyles}
                    title={voteBtnTitle}
                    aria-label={voteBtnTitle}
                    onClick={this.handleVoteClick}
                >
                    <Next
                        className="player__ctrlsVoteIcon"
                        aria-hidden="true"
                    />
                </button>
                <button
                    className={likeBtnStyles}
                    aria-label="Нравится"
                    onClick={this.handleLikeClick}
                >
                    <Heart
                        className="player__ctrlsLikeIcon"
                        aria-hidden="true"
                    />
                </button>
            </div>
        );
    };

    render() {
        const { currentTrack } = this.props;

        return (
            <div className="player">
                {this.renderHeader()}
                <TrackInfo currentTrack={currentTrack} />
                {this.renderControls()}
                <button
                    className="player__logout"
                    onClick={this.handleLogoutClick}
                    aria-label="Выйти"
                >
                    <Logout className="player__logoutIcon" aria-hidden={true} />
                    <span className="player__logoutText">Отключиться</span>
                </button>
            </div>
        );
    }
}
