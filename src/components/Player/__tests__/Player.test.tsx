import * as React from "react";
import { Role } from "interfaces";
import { shallow } from "enzyme";
import { Player } from "../";

const playerProps = {
    currentTrack: null,
    role: Role.Listener,
    isPlaying: true,
    peopleInRoom: 1,
    votes: 0,
    isVoted: true
};

describe("Компонент Player", () => {
    describe("Громкоговоритель", () => {
        it("Если пришло 0 голосов или меньше, компонент не отрендерится", () => {
            const wrapperWithZeroVotes = shallow(<Player {...playerProps} />);
            const wrapperWithLessZeroVotes = shallow(
                <Player {...playerProps} votes={-1} />
            );

            expect(wrapperWithZeroVotes.find(".votes").length).toBe(0);
            expect(wrapperWithLessZeroVotes.find(".votes").length).toBe(0);
        });

        it("Если пришло 1 и больше голосов, компонент отрендерится", () => {
            const wrapperWithOneVote = shallow(
                <Player {...playerProps} votes={1} />
            );
            const wrapperWithTwoVotes = shallow(
                <Player {...playerProps} votes={2} />
            );

            expect(wrapperWithOneVote.find(".votes").length).toBe(1);
            expect(wrapperWithTwoVotes.find(".votes").length).toBe(1);
        });
    });
});
