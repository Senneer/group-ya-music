import * as React from "react";
import "./ErrorPage.css";

export class ErrorPage extends React.PureComponent<{}, {}> {
    render() {
        return (
            <div className="errorPage">
                <p className="errorPage__msg">
                    Упс. Не смогли найти вкладку с Яндекс.Музыкой.
                </p>
            </div>
        );
    }
}
