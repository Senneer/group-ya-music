import * as React from 'react';

interface IProps {
    className?: string;
}

export const Logout = (props: IProps) => {
    const { className, ...restProps } = props;

    return (
        <svg xmlns='http://www.w3.org/2000/svg' width='20' height='20' viewBox='0 0 512 512' className={className} {...restProps}>
            <path d='M255.15 468.625H63.787c-11.737 0-21.262-9.526-21.262-21.262V64.638c0-11.737 9.526-21.262 21.262-21.262H255.15c11.758 0 21.262-9.504 21.262-21.262S266.908.85 255.15.85H63.787C28.619.85 0 29.47 0 64.638v382.724c0 35.168 28.619 63.787 63.787 63.787H255.15c11.758 0 21.262-9.504 21.262-21.262 0-11.758-9.504-21.262-21.262-21.262z' fill='currentColor' />
            <path d='M505.664 240.861L376.388 113.286c-8.335-8.25-21.815-8.143-30.065.213s-8.165 21.815.213 30.065l92.385 91.173H191.362c-11.758 0-21.262 9.504-21.262 21.262 0 11.758 9.504 21.263 21.262 21.263h247.559l-92.385 91.173c-8.377 8.25-8.441 21.709-.213 30.065a21.255 21.255 0 0 0 15.139 6.336c5.401 0 10.801-2.041 14.926-6.124l129.276-127.575A21.303 21.303 0 0 0 512 255.998c0-5.696-2.275-11.118-6.336-15.137z' fill='currentColor' />
        </svg>
    );
};

export const Play = (props: IProps) => {
    const { className, ...restProps } = props;

    return (
        <svg width='24' height='24' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 24 24' className={className} fillRule='evenodd' clipRule='evenodd' strokeLinejoin='round' strokeMiterlimit='1.414' {...restProps}>
            <path fill='none' d='M0 0h24v24H0z' />
            <path d='M6.857 2.571L22.286 12 6.857 21.429V2.571z' fill='currentColor' />
        </svg>
    );
};

export const Pause = (props: IProps) => {
    const { className, ...restProps } = props;

    return (
        <svg width='24' height='24' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 24 24' className={className} fillRule='evenodd' clipRule='evenodd' strokeLinejoin='round' strokeMiterlimit='1.414' {...restProps}>
            <path fill='none' d='M0 0h24v24H0z' />
            <path d='M5.143 3.429h5.143v17.142H5.143V3.429zm8.571 0h5.143v17.142h-5.143V3.429z' fill='currentColor' />
        </svg>
    );
};

export const Next = (props: IProps) => {
    const { className, ...restProps } = props;

    return (
        <svg xmlns='http://www.w3.org/2000/svg' width='28' height='28' viewBox='0 0 28 28' className={className} {...restProps}>
            <g fill='none' fillRule='evenodd'>
                <path fill='currentColor' d='M3 7l10 7-10 7V7zm20 0h2v14h-2V7zM13 7l10 7-10 7V7z' />
                <path d='M0 0h28v28H0z' />
            </g>
        </svg>
    );
};

export const Man = (props: IProps) => {
    const { className, ...restProps } = props;

    return (
        <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 258.75 258.75' width='20' height='20' className={className} {...restProps}>
            <g fill='currentColor'>
                <circle cx='129.375' cy='60' r='60' />
                <path d='M129.375 150c-60.061 0-108.75 48.689-108.75 108.75h217.5c0-60.061-48.689-108.75-108.75-108.75z' />
            </g>
        </svg>
    );
};

export const Megaphone = (props: IProps) => {
    const { className, ...restProps } = props;

    return (
        <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 511.999 511.999' width='20' height='20' className={className} {...restProps}>
            <path d='M467.069 212.578L267.381 12.861c-11.777-11.748-30.828-11.609-42.451-.015-11.689 11.719-11.689 30.747 0 42.451L424.647 255c11.7 11.7 30.721 11.7 42.422 0 11.732-11.732 11.732-30.692 0-42.422zM210.115 82.905l-1.503 7.534c-10.587 52.958-36.317 103.269-71.118 144.046l108.647 108.647c40.772-34.794 90.39-61.215 143.35-71.815l7.548-1.503L210.115 82.905zM117.575 256.989l-74.253 74.238c-17.545 17.545-17.618 46.029 0 63.647l42.422 42.422c17.545 17.545 46.029 17.617 63.647 0l74.246-74.246-106.062-106.061zm10.606 116.675c-5.859 5.859-15.352 5.859-21.211 0-5.859-5.859-5.859-15.352 0-21.211l21.211-21.211c5.859-5.859 15.352-5.859 21.211 0 5.859 5.859 5.859 15.352 0 21.211l-21.211 21.211zM286.266 445.278l20.405-20.405c17.619-17.616 17.561-46.1.001-63.631l-15.156-15.167c-8.377 5.627-16.377 11.741-24.155 18.265l18.1 18.127c5.845 5.815 5.886 15.279 0 21.196l-20.742 20.742-30.482-29.533-42.424 42.424 68.057 65.947c11.614 11.644 30.683 11.71 42.407-.015 11.704-11.704 11.704-30.732 0-42.437l-16.011-15.513zM346.864 0c-8.291 0-15 6.709-15 15v30c0 8.291 6.709 15 15 15s15-6.709 15-15V15c0-8.291-6.709-15-15-15zM466.864 120h-30c-8.291 0-15 6.709-15 15s6.709 15 15 15h30c8.291 0 15-6.709 15-15s-6.709-15-15-15zM447.469 34.394c-5.859-5.859-15.352-5.859-21.211 0l-30 30c-5.859 5.859-5.859 15.352 0 21.211s15.352 5.86 21.211 0l30-30c5.859-5.859 5.859-15.352 0-21.211z' fill='currentColor' />
        </svg>
    );
};

export const Heart = (props: IProps) => {
    const { className, ...restProps } = props;

    return (
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" className={className} {...restProps}>
            <path stroke="currentColor" strokeWidth="2px" fillRule="evenodd" d="M12 21c.71-.75 4.762-4.628 7.296-7.306 2.27-2.401 2.222-5.9.157-8.071-2.063-2.172-5.4-2.163-7.453.021-2.053-2.184-5.39-2.193-7.454-.021-2.064 2.17-2.11 5.67.158 8.071C7.237 16.372 11.29 20.25 12 21z" />
        </svg>
    );
};
