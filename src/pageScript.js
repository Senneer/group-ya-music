const Action = require('constants/ActionTypes');

console.log('Group Ya Music extension connected.');

function switchSong() {
    externalAPI.next();
};

function sendDataOnSwitch() {
    const track = externalAPI.getCurrentTrack();
    const isPlaying = externalAPI.isPlaying();

    window.postMessage({
        action: Action.YA_STATE_CHANGED,
        isPlaying,
        track,
    });
};

function togglePlay() {
    externalAPI.togglePause();
};

function sendInitData() {
    const track = externalAPI.getCurrentTrack();
    const isPlaying = externalAPI.isPlaying();

    window.postMessage({
        action: Action.GET_INIT_DATA_FROM_PAGE,
        track,
        isPlaying,
    });
};

function toggleLike() {
    externalAPI.toggleLike().then(sendInitData);
}

if (!window.areListenersAdded) {
    window.switchSong = switchSong;
    window.sendTrackOnSwitch = sendDataOnSwitch;
    window.togglePlay = togglePlay;
    window.toggleLike = toggleLike;

    window.addEventListener(Action.SWITCH_SONG, window.switchSong);
    window.addEventListener(Action.TOGGLE_PLAY, window.togglePlay);
    window.addEventListener(Action.TOGGLE_LIKE, window.toggleLike);

    window.areListenersAdded = true;
}

sendInitData();

// EVENT_STATE отвечает за play/pause, next
// EVENT_CONTROLS — за like
// EVENT_TRACK — переключение трека
externalAPI.off(externalAPI.EVENT_STATE, window.sendTrackOnSwitch);
externalAPI.off(externalAPI.EVENT_TRACK, window.sendTrackOnSwitch);
externalAPI.off(externalAPI.EVENT_CONTROLS, window.sendTrackOnSwitch);
externalAPI.on(externalAPI.EVENT_STATE, window.sendTrackOnSwitch);
externalAPI.on(externalAPI.EVENT_TRACK, window.sendTrackOnSwitch);
externalAPI.on(externalAPI.EVENT_CONTROLS, window.sendTrackOnSwitch);
