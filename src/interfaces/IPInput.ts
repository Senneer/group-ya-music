export interface IPInputState {
    value: string,
    error: string,
};
