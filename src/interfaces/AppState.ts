import { Track } from "./";

export interface IAppState {
    role: RoleState;
    page: Page;
    currentTrack: TrackState;
    isPlaying: boolean;
    peopleInRoom: number;
    votes: number;
    isVoted: boolean;
}

export enum Page {
    IPInput = "IPInput",
    Player = "Player",
    Error = "Error"
}

export enum Role {
    Host = "Host",
    Listener = "Listener"
}

export type RoleState = Role | null;

export type TrackState = Track | null;
