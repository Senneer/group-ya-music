export interface Album {
    title: string;
    year: number;
    link: string;
    artists: any[];
    cover: string;
}

export interface Artist {
    title: string;
    link: string;
    cover: string;
}

export interface Track {
    title: string;
    artists: Artist[];
    album: any;
    cover: string;
    duration: number;
    liked: boolean;
    disliked: boolean;
    link: string;
    version: any;
}
