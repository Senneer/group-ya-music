const webpack = require("webpack");
const config = require("./client.config");

delete config.chromeExtensionBoilerplate;

webpack(config, function(err) {
    if (err) throw err;
});
