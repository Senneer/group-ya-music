const path = require("path");
const merge = require("webpack-merge");
const NodemonPlugin = require("nodemon-webpack-plugin");

const isProduction = process.env.NODE_ENV === "production";

const baseConfig = {
    context: path.resolve(__dirname, ".."),
    target: "node",
    entry: "./src/server/server",
    output: {
        path: path.resolve("./build"),
        filename: "server.js"
    },

    resolve: {
        modules: ["src", "node_modules"],
        extensions: [".ts", ".js"]
    },

    module: {
        rules: [
            {
                test: /\.ts$/,
                exclude: /node_modules/,
                loader: "awesome-typescript-loader"
            }
        ]
    },

    externals: {
        uws: "uws"
    },

    node: {
        fs: "empty"
    }
};

const dev = {
    mode: "development",

    plugins: [
        new NodemonPlugin({
            // Раскомментируй строку ниже если хочешь дебажить сервак
            // nodeArgs: ["--inspect"],
            watch: ["./build/server.js"],
            delay: 500
        })
    ]
};

const prod = {
    mode: "production"
};

const config = isProduction ? merge(baseConfig, prod) : merge(baseConfig, dev);

module.exports = config;
