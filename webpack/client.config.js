const webpack = require("webpack");
const path = require("path");
const env = require("./env");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const WriteFilePlugin = require("write-file-webpack-plugin");
const ExtensionReloader = require("webpack-extension-reloader");
const merge = require("webpack-merge");
const autoprefixer = require("autoprefixer");
const postcssNested = require("postcss-nested");

const isProduction = process.env.NODE_ENV === "production";

const baseConfig = {
    context: path.resolve(__dirname, ".."),
    entry: {
        popup: "./src/popup",
        contentScript: "./src/contentScript",
        pageScript: "./src/pageScript",
        options: "./src/options",
        background: "./src/background"
    },

    output: {
        path: path.resolve(__dirname, "../build"),
        filename: "[name].js"
    },

    resolve: {
        modules: ["src", "node_modules"],
        extensions: [".ts", ".tsx", ".js"]
    },

    module: {
        rules: [
            {
                test: /\.tsx?$/,
                exclude: /node_modules/,
                loader: "awesome-typescript-loader"
            },

            {
                test: /\.css$/,
                use: [
                    "style-loader",
                    "css-loader",
                    {
                        loader: "postcss-loader",
                        options: {
                            plugins: [autoprefixer(), postcssNested]
                        }
                    }
                ],
                exclude: /node_modules/
            }
        ]
    },

    plugins: [
        new webpack.DefinePlugin({
            "process.env.NODE_ENV": JSON.stringify(env.NODE_ENV)
        }),

        new HtmlWebpackPlugin({
            template: "./src/popup.html",
            filename: "popup.html",
            chunks: ["popup"]
        }),

        new CopyWebpackPlugin([
            {
                from: "./src/assets/manifest.json",
                transform: function(content) {
                    // generates the manifest file using the package.json informations
                    return Buffer.from(
                        JSON.stringify({
                            ...JSON.parse(content.toString()),
                            description: process.env.npm_package_description,
                            version: process.env.npm_package_version
                        })
                    );
                }
            },
            {
                from: "./src/assets/img"
            }
        ]),

        new WriteFilePlugin()
    ]
};

const dev = {
    mode: "development",

    devtool: "cheap-source-map",

    plugins: [
        new ExtensionReloader({
            port: 9090,
            reloadPage: false,
            entries: {
                contentScript: "contentScript",
                background: "background"
            }
        })
    ]
};

const prod = {
    mode: "production"
};

const config = isProduction ? merge(baseConfig, prod) : merge(baseConfig, dev);

module.exports = config;
